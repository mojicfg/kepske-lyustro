#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "generate.h"
#include "fileio.h"

int main(int argc, char **argv) {
	if (argc != 1 && argc != 2) {
		fputs("Wrong number of arguments\n", stderr);
		fputs("Usage: generate [output-file]\n", stderr);
		exit(EXIT_FAILURE);
	}
	read_config("config");
	srandom(time(NULL));
	generate();
	write_file(argc == 2? argv[1]: "matrix.txt");
	return EXIT_SUCCESS;
}

