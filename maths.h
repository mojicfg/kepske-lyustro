#pragma once
#include <math.h>

#include "params.h"

struct point_t {
	double i, j;
};

extern int grid_nm;
extern const double cell_nm;
extern const double TAU;

char valid_idx(int);
char valid_idxs(int, int);

double idx_to_crd(int);

double min(double, double);
double max(double, double);

double dist(const struct point_t, const struct point_t);
double vecsize(const struct point_t);

double angle_cos(const struct point_t, const struct point_t);

struct point_t vec_fromto(const struct point_t, const struct point_t);

double random01();
double randompm();
double randompm3();
double randompm_normal();

double random_angle();
double random_curvature_angle();

double interpolate(const double, const double, const double);

