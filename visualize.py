from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
import sys

with open('config', 'r') as cfg:
    SIZE_NM = int(cfg.readline())
    ZRANGE_NM = SIZE_NM // 2
    size = int(cfg.readline())
    z_scale = int(cfg.readline())

if len(sys.argv) < 2:
    print('No file name specified.')
    sys.exit()

SCALE = 1000
Z_EXTRA = 10

with open(sys.argv[1]) as f:
    x = np.outer(np.linspace(0, SIZE_NM / SCALE, size), np.ones(size))
    y = x.copy().T # transpose
    z = np.array([list(map(lambda x: float(x) / SCALE * z_scale, f.readline().split())) for _ in range(size)])

fig = plt.figure() #figsize=(3, 3))
ax = plt.axes(projection='3d')
ax.plot_surface(x, y, z, cmap=plt.get_cmap('binary'))
ax.set_xlim([0, SIZE_NM / SCALE])
ax.set_ylim([0, SIZE_NM / SCALE])
ax.set_zlim([-ZRANGE_NM / SCALE / Z_EXTRA, ZRANGE_NM / SCALE / Z_EXTRA])
ax.set_xlabel('X (µm)')
ax.set_ylabel('Y (µm)')
ax.set_zlabel('Z (µm)')

#with open('debug') as f:
#    for line in f.readlines():
#        data = line.split()
#        if data[0] == 'point':
#            x, y = map(float, data[1:])
#            #ax.scatter(x, y, 50, depthshade=False)
#            ax.plot([x], [y], [10], markerfacecolor='r', markeredgecolor='k', marker='o', markersize=20, alpha=1)

plt.show()

