#include <assert.h>
#include <stdlib.h>
#include "params.h"
#include "maths.h"

const double TAU = M_PI + M_PI;

char valid_idx(int i) {
	return 0 <= i && i < SIZE;
}
char valid_idxs(int i, int j) {
	return valid_idx(i) && valid_idx(j);
}

double idx_to_crd(int i) {
	const double cell_nm = grid_nm / (double) SIZE;
	return cell_nm * i;
}

double min(double a, double b) {
	return a < b? a : b;
}
double max(double a, double b) {
	return a > b? a : b;
}

double dist(const struct point_t a, const struct point_t b) {
	return sqrt((a.i - b.i) * (a.i - b.i) + (a.j - b.j) * (a.j - b.j));
}
double vecsize(const struct point_t v) {
	return sqrt(v.i * v.i + v.j * v.j);
}

double angle_cos(const struct point_t v1, const struct point_t v2) {
	return (v1.i * v2.i + v1.j * v2.j) / vecsize(v1) / vecsize(v2);
}

struct point_t vec_fromto(const struct point_t a, const struct point_t b) {
	return (struct point_t){.i = b.i - a.i, .j = b.j - a.j};
}

double random01() {
	return random() / (double) RAND_MAX;
}
double randompm() {
	return random01() * 2 - 1;
}
double randompm3() {
	const double r = randompm();
	return r * r * r;
}
#define EPS 1e-7
double randompm_normal() {
	// based on the Box-Muller method
	double u = random01(), v = random01();
	if (u < EPS)
		u += EPS;
	if (v < EPS)
		v += EPS;
	const double x = sqrt(-2. * log(u)) * cos(TAU * v) / 4.;
	return max(min(x, 1.), -1.);
}
#undef EPS

double random_angle() {
	return random01() * TAU;
}
double random_curvature_angle() {
	return randompm() * MAX_REGULAR_CURVATURE_ANGLE;
}

double _interpolate_sin(const double f) {
	assert(f >= 0 && f <= 1);
	return (sin((f + f - 1) * TAU / 4.) + 1) / 2.;
}
double interpolate(const double a, const double b, const double f) {
	assert(f >= 0 && f <= 1);
	return a + _interpolate_sin(_interpolate_sin(f)) * (b - a);
	// linear: return a + (b - a) * f;
}

