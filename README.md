# Kepske Lyustro
(from Ukrainian *кепське люстро* for "a crappy mirror")

## Configuring

The `config` file contains all of the parameters exposed to the user.

One can edit `params.h` to tinker with the algorithm's inner parameters.
This is discouraged, though, as doing so without caution may lead to generated results being unrealistic, or runtime errors.

## Building

The project uses `make` for building.
```sh
make 'CFLAGS+=-DGENERATE_SCRATCH -DGENERATE_THINGIES -DGENERATE_EFFECTS'
make clean
```
The `GENERATE_*` defines are optional. Removing them disables some of the program's functionality.

Consider adding `-Drandom=rand -Dsrandom=srand` if your OS does not provide `random()` and `srandom()` C functions.

