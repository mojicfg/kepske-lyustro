#pragma once

extern int scale_by; // divide the resulting matrix by this value for scaling purposes

extern int grid_nm;

#define MAX_SIZE 1000
extern int SIZE;
extern double t[MAX_SIZE][MAX_SIZE];

/* [ Profile ] */
#define PF_MAXV 6
#define PF_MAXH 1100
#define PF_MINH -1300
#define PF_OFFSET_MAXD 2500
#define PF_OFFSET_MIND 560
#define PF_FIRST_OFFSET 2500
#define PF_APPLY_DEPTH 4000 // should be greater than STEP_SIZE
#define PF_OFFSET_MAXALTER 56
#define PF_HEIGHT_MAXALTER 90

/* [ Trajectory ] */
#define STEP_SIZE 3000
//#define MAX_STEPS 100
extern int len_max;
//#define MIN_STEPS 20
extern int len_min;
#define FADEOUT_STEPS 30

#define MAX_REGULAR_CURVATURE_ANGLE 0.0207345 //(TAU * .0033)

/* [ Rapid Turn ] */
#define RAPID_TURN_CHANCE 10
#define RAPID_NECESSARY_STEPS 20
#define RAPID_LEN_MIN 10000
#define RAPID_LEN_MAX 13000
#define MIN_RAPID_CURVATURE_ANGLE 0.25
#define MAX_RAPID_CURVATURE_ANGLE 0.5

/* [ Thingies ] */
extern int min_thingies;
extern int max_thingies;

#define TH_MAXH 280
#define TH_MINRING 2 // ring amt range
#define TH_MAXRING 5
#define TH_MINRINGNM 5 // ring size range
#define TH_MAXRINGNM 400 // keeping these close makes it more circular

/* [ Effects ] */
#define NOISE_DELTA 100
#define FILTER_MARGIN 2
#define FILTER_SIZE (FILTER_MARGIN + FILTER_MARGIN + 1)
extern double effect[MAX_SIZE + FILTER_SIZE - 1][MAX_SIZE + FILTER_SIZE - 1];
extern int filter_invscalar;
extern int filter[FILTER_SIZE][FILTER_SIZE];

