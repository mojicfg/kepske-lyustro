#pragma once

enum fio_rc {
	FIO_OK,
};

enum fio_rc read_config(char*);
enum fio_rc write_file(char*);

