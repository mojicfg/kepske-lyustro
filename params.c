#include "params.h"

int scale_by;

int grid_nm;

int SIZE;
double t[MAX_SIZE][MAX_SIZE];

int len_max;
int len_min;

int min_thingies;
int max_thingies;

double effect[MAX_SIZE + FILTER_SIZE - 1][MAX_SIZE + FILTER_SIZE - 1];

int filter[FILTER_SIZE][FILTER_SIZE] = {
	{1, 4, 6, 4, 1,},
	{4, 16, 24, 16, 4,},
	{6, 24, 36, 24, 6,},
	{4, 16, 24, 16, 4,},
	{1, 4, 6, 4, 1,},
};
int filter_invscalar = 256;

