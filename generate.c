#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "generate.h"

void /*deprecated*/ apply_sphere(const double oi, const double oj) {
	const double r = 30;
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			const double ci = idx_to_crd(i);
			const double cj = idx_to_crd(j);
			const double d = dist((struct point_t){ci, cj}, (struct point_t){oi, oj});
			if (d < r) {
				const double h = sqrt(r * r - d * d);
				t[i][j] = min(t[i][j], -h);
			}
		}
	}
}

int profile_size = 0;
struct point_t profile[PF_MAXV]; // .i = offset, .j = height
struct point_t profile_bak[PF_MAXV];

#define EPS 1e-3
void generate_profile() {
	// leftmost and rightmost always have the height of 0 for smooth transition
	profile_size = random() % (PF_MAXV - 2) + 3;
	profile[0] = (struct point_t){.i = -PF_FIRST_OFFSET, .j = 0};
	int positive_amt = 0;
	for (int i = 1; i < profile_size; ++i) {
		profile[i].i = profile[i - 1].i + PF_OFFSET_MIND + random01() * (PF_OFFSET_MAXD - PF_OFFSET_MIND);
		profile[i].j = i + 1 == profile_size? 0 : PF_MINH + random01() * (PF_MAXH - PF_MINH);
		if (profile[i].j > EPS) {
			++positive_amt;
		}
	}
	if (positive_amt == 0) {
		// Always have at least one positive h-value.
		profile[1].j = -profile[1].j;
	}
	for (int i = 0; i < profile_size; ++i) {
		profile_bak[i] = profile[i];
	}
}
#undef EPS
void alter_profile() {
	for (int i = 0; i < profile_size; ++i) {
		const double offset_delta = randompm3() * PF_OFFSET_MAXALTER;
		const double height_delta = randompm3() * PF_HEIGHT_MAXALTER;
		profile[i].i += offset_delta;
		if (i > 0) // a simple and slightly crappy way to avoid collisions
			profile[i].i = max(profile[i].i, profile[i - 1].i);
		if (i != 0 && i + 1 != profile_size)
			profile[i].j += height_delta;
	}
}
void invert_profile() {
	for (int i = 0; i < profile_size; ++i) {
		profile[i].i = -profile_bak[i].i;
		profile[i].j = profile_bak[i].j;
	}
	for (int i = 0; i + i < profile_size; ++i) {
		const int j = profile_size - i - 1;
		struct point_t tmp = profile[i];
		profile[i] = profile[j];
		profile[j] = tmp;
	}
}

void apply_profile(const struct point_t c, const struct point_t v, const double fadeout_mod) {
	const struct point_t norm = {.i = v.j, .j = -v.i};
	const struct point_t invn = {.i = -norm.i, .j = -norm.j};
	struct point_t p1;
	double h1;
	for (int i = 0; i < profile_size; ++i) {
		const double offset = profile[i].i;
		const struct point_t p2 = {.i = c.i + norm.i * offset, .j = c.j + norm.j * offset};
		const double h2 = profile[i].j;
		if (i > 0) {
			for (int i = 0; i < SIZE; ++i) {
				for (int j = 0; j < SIZE; ++j) {
					const struct point_t p = {.i = idx_to_crd(i), .j = idx_to_crd(j)}; const struct point_t v1 = vec_fromto(p1, p);
					const struct point_t v2 = vec_fromto(p2, p);
					const double cos1 = angle_cos(v1, norm);
					const double cos2 = angle_cos(v2, invn);
					if (cos1 > 0 && cos2 > 0) {
						const double sin1 = sqrt(1 - cos1 * cos1);
						const double delta = vecsize(v1) * cos1;
						const double lateral = vecsize(v1) * sin1;
						if (lateral <= PF_APPLY_DEPTH) {
							const double h = interpolate(h1, h2, delta / dist(p1, p2)) * fadeout_mod;
							if (h > 0)
								t[i][j] = max(t[i][j], h);
							else
								t[i][j] = min(t[i][j], h);
						}
					}
				}
			}
		}
		p1 = p2;
		h1 = h2;
	}
}

void apply_trajectory(struct point_t c, double ca) {
	const int min_steps = len_min / 2. / (double) STEP_SIZE; // dividing by 2 since `apply_trajectory`
	const int max_steps = len_max / 2. / (double) STEP_SIZE; // is actually called twice in `generate`
	const int target_steps = min_steps + random01() * (max_steps - min_steps) + FADEOUT_STEPS;
	for (int steps_amt = 0; steps_amt < target_steps;) {
		//fprintf(stderr, "point %lf %lf\n", c.i, c.j);
		int steps = random() % max_steps;
		double angle = random_curvature_angle();
		if (target_steps - steps_amt > RAPID_NECESSARY_STEPS && random() % 100 < RAPID_TURN_CHANCE) {
			//fprintf(stderr, "rapid turn\n");
			const int min_steps = RAPID_LEN_MIN / (double) STEP_SIZE;
			const int max_steps = RAPID_LEN_MAX / (double) STEP_SIZE;
			steps = min_steps + random01() * (max_steps - min_steps);
			angle = MIN_RAPID_CURVATURE_ANGLE + random01() * (MAX_RAPID_CURVATURE_ANGLE - MIN_RAPID_CURVATURE_ANGLE);
			if (random() % 2 == 0)
				angle = -angle;
		}
		for (int i = 0; i < steps; ++i, ++steps_amt) {
			ca += angle;
			const double vi = cos(ca), vj = sin(ca);
			c.i += vi * STEP_SIZE, c.j += vj * STEP_SIZE;
			//apply_sphere(c.i, c.j);
			double fadeout_mod = 1.;
			if (target_steps <= steps_amt) {
				fadeout_mod = 0.;
			} else if (target_steps - steps_amt < FADEOUT_STEPS) {
				fadeout_mod = (target_steps - steps_amt) / (double) FADEOUT_STEPS;
			}
			apply_profile((struct point_t){.i = c.i, .j = c.j}, (struct point_t){.i = vi, .j = vj}, fadeout_mod);
			alter_profile();
		}
	}
}

char u[MAX_SIZE][MAX_SIZE];
struct queue_t {
	int i, j;
	int ring;
	double prev_ring_dist;
	struct queue_t *next;
} *queue_begin = NULL, *queue_end = NULL;
void queue_pop() {
	struct queue_t *tmp = queue_begin;
	queue_begin = queue_begin->next;
	free(tmp);
}
void queue_push(const struct queue_t el) {
	queue_end->next = (struct queue_t *)malloc(sizeof(struct queue_t));
	queue_end = queue_end->next;
	*queue_end = el;
}
int rings_amt;
double rings_h[TH_MAXRING];
void generate_rings_info() {
	rings_amt = TH_MINRING + random() % (TH_MAXRING - TH_MINRING + 1);
	rings_h[0] = random01() * TH_MAXH;
	for (int i = 1; i < rings_amt; ++i) {
		rings_h[i] = random01() * rings_h[i - 1];
	}
}
void generate_thingy(const int si, const int sj) {
	generate_rings_info();
	for (int i = 0; i < SIZE; ++i) {
		memset(u[i], 0, sizeof(char) * SIZE);
	}
	u[si][sj] = 1;
	queue_end = queue_begin = (struct queue_t *)malloc(sizeof(struct queue_t));
	*queue_begin = (struct queue_t){.i = si, .j = sj, .next = NULL,
		.ring = 0,
		.prev_ring_dist = 0.,
	};
	while (queue_begin != NULL) {
		const struct queue_t q = *queue_begin;
		t[q.i][q.j] = rings_h[q.ring];
		for (int i = q.i - 1; i <= q.i + 1; ++i) {
			for (int j = q.j - 1; j <= q.j + 1; ++j) {
				if (((i == q.i) ^ (j == q.j)) && valid_idxs(i, j) && !u[i][j]){
					const double d = dist(
							(struct point_t){.i = idx_to_crd(i), .j = idx_to_crd(j)},
							(struct point_t){.i = idx_to_crd(si), .j = idx_to_crd(sj)}
							) - q.prev_ring_dist;
					u[i][j] = 1;
					int ring = q.ring;
					double prev_ring_dist = q.prev_ring_dist;
					const double ring_nm = d - prev_ring_dist;
					if (ring_nm > TH_MINRINGNM) {
						const int chance = 100 * ((ring_nm - TH_MINRINGNM) / (double)(TH_MAXRINGNM - TH_MINRINGNM));
						// `chance` may as well go over 100%, no problem there but worth keeping in mind
						if (random() % 100 < chance) {
							++ring;
							prev_ring_dist = d;
						}
					}
					if (ring < rings_amt) {
						queue_push((struct queue_t){.i = i, .j = j, .ring = ring,
								.prev_ring_dist = prev_ring_dist,
								});
					}
				}
			}
		}
		queue_pop();
	}
}
#include <assert.h>
void generate_noise() {
	//double test_max = -5;
	//double test_min = 5;
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			const double tmp = randompm_normal();
			//test_max = max(test_max, tmp);
			//test_min = min(test_min, tmp);
			assert(-1 <= tmp && tmp <= 1);
			effect[i][j] = tmp * NOISE_DELTA;
		}
	}
}
void apply_filter() {
	/*int invscalar_check = 0;
	for (int i = 0; i < FILTER_SIZE; ++i) {
		for (int j = 0; j < FILTER_SIZE; ++j) {
			invscalar_check += filter[i][j];
		}
	}*/
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			double acc = 0;
			for (int fi = 0; fi < FILTER_SIZE; ++fi) {
				for (int fj = 0; fj < FILTER_SIZE; ++fj) {
					acc += filter[fi][fj] * effect[i + fi][j + fj];;
				}
			}
			acc /= (double) filter_invscalar;
			t[i][j] += acc;
		}
	}
}
void scale_down() {
	if (scale_by == 1)
		return;
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			t[i][j] /= (double) scale_by;
		}
	}
}
void generate() {
	// Generating a scratch
#ifdef GENERATE_SCRATCH
	generate_profile();
	struct point_t c = {idx_to_crd(random() % SIZE), idx_to_crd(random() % SIZE)};
	double ca = random_angle();
	apply_trajectory(c, ca);
	ca -= TAU / 2.;
	invert_profile();
	apply_trajectory(c, ca);
#endif
	// Generating small thingies
#ifdef GENERATE_THINGIES
	const int thingies_amt = min_thingies + random() % (max_thingies - min_thingies + 1);
	for (int i = 0; i < thingies_amt; ++i) {
		const double ci = random() % SIZE;
		const double cj = random() % SIZE;
		generate_thingy(ci, cj);
	}
#endif
	// Applying additional effects
#ifdef GENERATE_EFFECTS
	generate_noise();
	apply_filter();
#endif
	scale_down();
}

