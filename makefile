C=gcc
CFLAGS=-std=gnu17

main: fileio.o generate.o main.o maths.o params.o
	$C -ogenerate -lm $^

clean:
	rm *.o

fileio.o: fileio.c fileio.h maths.h params.h
	$C -c $< $(CFLAGS)

generate.o: generate.c generate.h maths.h params.h
	$C -c $< $(CFLAGS)

main.o: main.c generate.h fileio.h
	$C -c $< $(CFLAGS)

maths.o: maths.c maths.h params.h
	$C -c $< $(CFLAGS)

params.o: params.c params.h
	$C -c $< $(CFLAGS)

