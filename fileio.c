#include <stdio.h>
#include "params.h"
#include "fileio.h"

enum fio_rc set_from_cfg(FILE *cfg, int *var) {
	fscanf(cfg, "%d", var); // XXX
	return FIO_OK;
}

enum fio_rc read_config(char *filename) {
	FILE *cfg = fopen(filename, "r"); // XXX
	set_from_cfg(cfg, &grid_nm);
	set_from_cfg(cfg, &SIZE);
	set_from_cfg(cfg, &scale_by);
	set_from_cfg(cfg, &len_min);
	set_from_cfg(cfg, &len_max);
	set_from_cfg(cfg, &min_thingies);
	set_from_cfg(cfg, &max_thingies);
	fclose(cfg);
	return FIO_OK;
}

enum fio_rc write_file(char *filename) {
	FILE *output = fopen(filename, "w"); // XXX
	//fprintf(output, "%d\n", SIZE);
	for (int i = 0; i < SIZE; ++i) {
		for (int j = 0; j < SIZE; ++j) {
			fprintf(output, "%.6lf", t[i][j]);
			fputs(j + 1 == SIZE? "\n": " ", output);
		}
	}
	fclose(output);
	return FIO_OK;
}

